<?php 
use Imagen;
// crear dos clases que hereden de la clase Imagen
// Articulo y ArticuloRebajado que sera una subclase del Artículo
// la clase artículo contendrá dos atributos protegidos
// nombre tipo string a null por defecto y precio tipo float a 0 por defecto
// el constructor recibirá como como parámetros opcionales el nombre y el precio
// le asignara dichos valores a sus correspondientes atributos
// en caso de no pasarle esos valores cogerán sus valores predeterminados

class Articulo extends Imagen
{
    protected $nombre;
    protected $precio;
    public function __construct($src, $border = null, $ancho = null, $alto = null, $nombre = null, $precio = 0)
    {
        parent::__construct($src, $border, $ancho, $alto);
        $this->nombre = $nombre;
        $this->precio = $precio;
    }
   // crear los geter y setters para el precio y el nombre
// get precio que devuelva el valor actual del precio
    public function getPrecio()
    {
        return $this->precio;
    } 
 // set precio($spPrecio)que compruebe si la variable $spPrecio es un valor numérico
 // y si es así, se lo asigne al atributo precio de la clase Articulo

    public function setPrecio($sprecio)
    {
        if (is_numeric($sprecio)) {
            $this->precio = $sprecio;
        }
    }
    // get nombre que devuelve el valor del nombre
    public function getNombre()
    {
        return $this->nombre;
    }
    // set nombre que le asigna un valor al nombre
    public function setNombre($snombre)
    {
        $this->nombre = $snombre;
    }
}

// crea la clase Articulo Rebajado que será declarada como final y no se podrá heredar
// de ella y será hija de clase Artículo
// esta clase contendrá  un atributo llamado $rebaja
// el constructor por defecto que recibe como parámetros obligatorios
// $spNombre, $spPrecio, $srebaja. Dentro de dicho constructor habra que llamar al constructor
// padre para darle valor al nombre y al precio
// despues el valor al atributo rebaja

class ArticuloRebajado extends Articulo
{
    private $rebaja;
    public function __construct($src, $border = null, $ancho = null, $alto = null, $nombre = null, $precio = 0, $rebaja = 0)
    {
        parent::__construct($src, $border, $ancho, $alto, $nombre, $precio);
        $this->rebaja = $rebaja;
    }
    public function getRebaja()
    {
        return $this->rebaja;
    }
    public function setRebaja($srebaja)
    {
        $this->rebaja = $srebaja;
    }
    // un método privado llamado CalculaDescuento que nos devuelve el precio de la 
    // rebaja dividido entre 100.
    private function calculaDescuento()
    {
        return $this->rebaja/100;
    }
    // un metodo publico llamado precioRebajado que nos devuelve la diferencia entre el precio
    // y el descuento
    public function precioRebajado()
    {
        return $this->precio - ($this->precio * $this->calculaDescuento());
    }
    // un método _toString del padre junto con la cadena de texto "La rebaja es: ".$this->rebaja.
    // '% y el descuento es '.$this->calculaDescuento().'€'

    public function __toString()
    {
        return "La rebaja es: ".$this->rebaja."% y el descuento es ".$this->calculaDescuento()."€";
    }
}


