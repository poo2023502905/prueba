<?php 


class Imagen
{
    private $src;
    private $border;
    private $ancho;
    private $alto;
// hacer el constructor
    public function __construct($src, $border = null, $ancho = null, $alto = null)
    {
        $this->src = $src;
        $this->border = $border;
        $this->ancho = $ancho;
        $this->alto = $alto;
    }
    public function __toString()
    {
        return "<img src='$this->src' border='$this->border' width='$this->ancho' height='$this->alto'>";

    }
// insertar los getters y los setters

    public function getSrc()
    {
        return $this->src;
    }

    public function setSrc($src)
    {
        // esta funcion debe comprobar con un if que la ruta que nos pasan como parámetro
        // coresponde con un fichero . Hax uso de la funcion "bool file_exists()"
        // ojo hay que comprobar que existe el fichero completo pero introduciendo la ruta completa
        // si no se cumple la comprobación muestra un mensaje de error terminando la ejecución
        // del script php. Puedes usar exit o die para terminar la ejecución del script php

        if (file_exists($src)) {
            $this->src = $src;
        } else {
            echo "El fichero $src no existe";
            exit;
        }
    }

    public function getBorder()
    {
        return $this->border;
    }

// comprobar que el borde que nos pasan como parámetro es un entero o 
// que sea mayor o igual que 0
// si no se cumple la comprobacion muestra un mensaje de error
// terminando la ejecución del sript php.
// Puedes usar exit o die para terminar la ejecución del script php
// esta función tiene que aparecer en el constructor para que compruebe que son
// los parametros correctos los parametros que nos pasan en el mismo
// en vez de $this->src=$pSrc lo cambias por $this->setSrc($pSrc)

    public function setBorder($border)
    {
        if (is_int($border) && $border >= 0) {
            $this->border = $border;
        } else {
            echo "El borde debe ser un entero mayor o igual que cero";
            exit;
        }
    }

    public function getAncho()
    {
        return $this->ancho;
    }

    public function setAncho($ancho)
    {
        $this->ancho = $ancho;
    }

    public function getAlto()
    {
        return $this->alto;
    }

    public function setAlto($alto)
    {
        $this->alto = $alto;
    }
 

  


}