<?php 


class Imagen
{
    private $src;
    private $border;
    private $ancho;
    private $alto;
// hacer el constructor
    public function __construct($src, $border = null, $ancho = null, $alto = null)
    {
        $this->src = $src;
        $this->border = $border;
        $this->ancho = $ancho;
        $this->alto = $alto;
    }
    public function __toString()
    {
        return "<img src='$this->src' border='$this->border' width='$this->ancho' height='$this->alto'>";

    }
// insertar los getters y los setters

    public function getSrc()
    {
        return $this->src;
    }

    public function setSrc($src)
    {
        $this->src = $src;
    }

    public function getBorder()
    {
        return $this->border;
    }

    public function setBorder($border)
    {
        $this->border = $border;
    }

    public function getAncho()
    {
        return $this->ancho;
    }

    public function setAncho($ancho)
    {
        $this->ancho = $ancho;
    }

    public function getAlto()
    {
        return $this->alto;
    }

    public function setAlto($alto)
    {
        $this->alto = $alto;
    }


}