<?php 
// desarrollar una clase que nos permita conectarnos a una base de datos y realizar consultas sobre ella
// la base de datos se denomina PruebaPhp

class Conexion
{
    private $servidor = "localhost";
    private $usuario = "root";
    private $password = "";
    private $basedatos = "PruebaPhp";
    private $conexion;

    public function __construct()
    {
        $this->conexion = new mysqli($this->servidor, $this->usuario, $this->password, $this->basedatos);
        if ($this->conexion->connect_error) {
            die("La conexión a la base de datos ha fallado: " . $this->conexion->connect_error);
        }
    }

    public function consulta($consulta)
    {
        $resultado = $this->conexion->query($consulta);
        if (!$resultado) {
            die("La consulta a la base de datos ha fallado: " . $this->conexion->error);
        }
        return $resultado;
    }

    public function __destruct()
    {
        $this->conexion->close();
    }
}

// crea una tabla denominada autores con los siguentes campos
// id, nombre, apellidos
//
// la clase denominada Autores debe permitir
// metodo insertar datos : introduce los datos pasados como argumento 
// en formato array asociativo
// metodo listar: devuelve todos los registros
// metodo eliminar: recibe el id del registro a eliminar y lo borra



class Autores
{
    private $id;
    private $nombre;
    private $apellidos;

    public function __construct($pId, $pNombre, $pApellidos)
    {
        $this->id = $pId;
        $this->nombre = $pNombre;
        $this->apellidos = $pApellidos;
    }
// un metodo insertar que introduce los datos pasados como un argumento
// en formato de array asociativo

    public function insertar($array)
    {
        $this->id = $array['id'];
        $this->nombre = $array['nombre'];
        $this->apellidos = $array['apellidos'];
    }

// un metodo listar

    public function listar()
    {
        $consulta = "SELECT * FROM autores";
        return $this->$consulta ;
    }
    // metodo eliminar: recibe el id del registro a eliminar y lo borra

    public function eliminar($id)
    {
        $consulta = "DELETE FROM autores WHERE id = $id";
        return $this->$consulta;
    }

}  