<?php
// creo una variable y la inicializo a 1//

$numero = 1;
$texto = "hola";//creando variable de tipo string
/*
fin de creacion de variables
*/
// 
$numero = $numero + 1;
$numero++;//postincremento primero asigna y despues suma
++$numero;//preincremento primero suma y despues asigna
$final = $numero++;
// otra forma de sumar uno, con un operador contraido
// vale para cualquier operador aritmetico
$numero += 1; // se puede poner cualquier numero
// mostrar el resultado
// con echo
//de otra manera

echo "La variable numero vale " . $numero . "<br>";
echo "La variable numero vale $numero<br>";
echo 'La variable numero vale $numero<br>';
echo "La variable numero vale {$numero}<br>";



