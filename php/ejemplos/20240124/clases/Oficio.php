<?php
class Oficio
{
    // atributos
    public string $nombre;
    public float $salarioBase;
    public int $horasSemanales;

    // constructor
    public function __construct(string $nombre = "", float $salarioBase = 0, int $horasSemanales = 0)
    {
        $this->nombre = $nombre;
        $this->salarioBase = $salarioBase;
        $this->horasSemanales = $horasSemanales;
    }

    // metodos
    // metodo calcular
    // devolver el sueldo total por semana
    public function calcular(): float
    {
        return ($this->salarioBase * $this->horasSemanales);
    }
}
