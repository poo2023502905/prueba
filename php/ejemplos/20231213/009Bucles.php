<?php

// crear una variable array
// donde voy a colocar cada vez que un jugador a
// metido un punto
// tengo 3 jugadores llamados
// p1, p2, p3
// quiero una tabla donde me indique el resultado
// de cada jugador
// realizarlo con for

$puntos = ['p1', 'p1', 'p2', 'p3', 'p2', 'p1', 'p1'];

// <table>
//     <tr>
//         <td>p1</td>
//         <td>4</td>
//     </tr>
//     <tr>
//         <td>p2</td>
//         <td>2</td>
//     </tr>
//     <tr>
//         <td>p3</td>
//         <td>1</td>
//     </tr>
// </table>

// utilizar array asociativo para almacenar los puntos
// de cada jugador

$jugadores = [
    'p1' => 0,
    'p2' => 0,
    'p3' => 0
];

// recorrer el array de puntos
// y mediante un switch almacenar los puntos 
// de cada jugador en el array de jugadores

// for ($i = 0; $i < count($puntos); $i++) {
//     switch ($puntos[$i]) {
//         case 'p1':
//             $jugadores['p1']++;
//             break;
//         case 'p2':
//             $jugadores['p2']++;
//             break;
//         case 'p3':
//             $jugadores['p3']++;
//             break;
//     }
// }

// otra opcion es no utiliar switch

for ($i = 0; $i < count($puntos); $i++) {
    $jugadores[$puntos[$i]]++;
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="estilos.css">
</head>

<body>
    <table>
        <tr>
            <th>P1</th>
            <td><?= $jugadores['p1'] ?></td>
        </tr>
        <tr>
            <th>P2</th>
            <td><?= $jugadores['p2'] ?></td>
        </tr>
        <tr>
            <th>P3</th>
            <td><?= $jugadores['p3'] ?></td>
        </tr>
    </table>
    <table>
        <?php
        foreach ($jugadores as $indice => $valor) {
            echo "<tr>
                    <th>{$indice}</th>
                    <td>{$valor}</td>
                </tr>";
        }
        ?>
    </table>
</body>

</html>