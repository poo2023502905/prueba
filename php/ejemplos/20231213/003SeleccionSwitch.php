<?php

// Crear una variable llamada unidades  que puede valer 
// 1-100
// Imprimir el descuento que se aplica a la compra en función de las unidades solicitadas
// <10 => 1%
// >=10 y <50 => 2%
// >=50 => 10%
// Realizarlo con switch

$unidades = 15;

switch (true) {
    case ($unidades < 10):
        echo "1%";
        break;
    case ($unidades < 50):
        echo "2%";
        break;
    default:
        echo "10%";
        break;
}


// realizarlo con if y elseif

if ($unidades < 10) {
    echo "1%";
} elseif ($unidades < 50) {
    echo "2%";
} else {
    echo "10%";
}
