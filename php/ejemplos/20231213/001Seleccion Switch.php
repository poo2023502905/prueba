<?php
// // selección multiple 
// if($i==0){
//     echo "es igual a 0";
// }else if($i==1){
//     echo "es igual a 1";
// }else if ($i==2){
//     echo "es igual a 2";
    
// }

// switch($i){
//     case 0:
//         echo "es igual a 0";
//         break;
//     case 1:
//         echo "es igual a 1";
//         break;
//     case 2:
//         echo "es igual a 2";
//         break;
// }
//n creamos una variable llamada mes
// con un numero del 1 al 12
// realizarlo utilizando switch
// mostrar el mes correspondiente enero
$mes=3;
switch($mes){
    case 1:
        echo "Enero";
        break;
    case 2:
        echo "Febrero";
        break;
    case 3:
        echo "Marzo";
        break;
    case 4:
        echo "Abril";
        break;
    case 5:
        echo "Mayo";
        break;
    case 6:
        echo "Junio";
        break;
    case 7:
        echo "Julio";
        break;
    case 8:
        echo "Agosto";
        break;
    case 9:
        echo "Septiembre";
        break;
    case 10:
        echo "Octubre";
        break;
    case 11:
        echo "Noviembre";
        break;
    case 12:
        echo "Diciembre";
        break;
    default:
        echo "Mes no valido";

    }
// otro ejemplo con la variable caso. Es importante poner el default
$caso =1;
switch($caso){
    case 1:
        echo "caso 1";
        break;
    case 2:
        echo "caso 2";
        break;
    case 3:
        echo "caso 3";
        break;
    default:
        echo "caso no valido";
}
// hacerlo con un if
$caso =1;
if($caso==1){
    echo "caso 1";
}else if($caso==2){
    echo "caso 2";
}else if($caso==3){
    echo "caso 3";
}else{
    echo "caso no valido";
}
// el calculo del descuento depende de la cantidad
// cantidad mayor a 10000 se aplica el 1%
// cantidad mayor a 5000 se aplica el 2%
// cantidad mayor a 1000 se aplica el 3%
// calculado con un if
$cantidad=1890;
if($cantidad>10000){
    $descuento=1/100;}
else if ($cantidad<5000){
    $descuento=2/100;}
    else($cantidad<1000){
        $descuento=3/100;}
    }
}
}

?>