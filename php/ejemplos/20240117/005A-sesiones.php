<?php

function aleatorios()
{
    $resultado = [];
    for ($c = 0; $c < 10; $c++) {
        $resultado[] = rand(1, 50);
    }
    return $resultado;
}

function aleatorios1()
{
    // crea un array con los 50 primeros numeros enteros
    $resultado = range(1, 50);
    // barajeo el array
    shuffle($resultado);
    // me quedo con los 10 primeros
    return array_slice($resultado, 0, 10);
}

// se trata de realizar un pequeño juego donde
// el usuario tiene que adivinar los 10 numeros del array numeros
// que debo crear con 10 numeros aleatorios entre 1 y 50
// creamos un formulario donde escribo un numero y cuando pulso
// enviar debe comprobar si el numero esta en el array
// si esta en el array me indica que he acertado y me lo muestra
// si fallo me cuenta un fallo y me debe mostrar el numero de fallos
// cuando adivine los 10 numeros la puntuacion sera el numero de intentos 
// para adivinar los 10 numeros

// una vez que adivina los 10 numeros debe borrar las variables de sesion
// colocar un boton para reiniciar el juego cuando quieras

session_start();

// mensaje por defecto
$mensaje = "";

// compruebo si no existe la variable de sesion
// esto solo lo hace la primera vez
if (!isset($_SESSION["aciertos"])) {
    // la primera vez que cargues la pagina
    // inicializo las variables de sesion
    $_SESSION["numeros"] = aleatorios();
    $_SESSION['aciertos'] = []; // numeros acertados
    $_SESSION['errores'] = 0; // numero de errores
    $_SESSION['intentos'] = 0; // numero de intentos
}

var_dump($_SESSION["numeros"]);

// ahora quiero comprobar si he pulsado el boton de jugar
if (isset($_POST["jugar"])) {
    // compruebo si el numero existe en el array de numeros
    // y no esta en el array de aciertos
    if (
        in_array($_POST["numero"], $_SESSION["numeros"]) // compruebo si el numero existe en el array de numeros
        &&
        !in_array($_POST["numero"], $_SESSION["aciertos"])  // compruebo si el numero no ha sido acertado anteriormente
    ) {
        // añado al array de aciertos el numero acertado
        $_SESSION['aciertos'][] = $_POST["numero"];

        // si gano sera aqui donde lo puedo saber
        if (count($_SESSION['aciertos']) == 10) {
            session_destroy();
            $mensaje = "Has ganado";
        }
    } else {
        // si no he acertado el numero sumo uno al contador de errores
        $_SESSION['errores']++;
    }
    // tanto si acierto como si fallo
    // sumo uno al contador de intentos
    $_SESSION['intentos']++;
} elseif (isset($_POST["reiniciar"])) {
    // He pulsado el boton de reiniciar

    // elimino las variables de session
    session_unset();
    // unset($_SESSION['aciertos']);
    // unset($_SESSION['errores']);
    // unset($_SESSION['intentos']);
    // unset($_SESSION['numeros']);

    // inicializo las variables de session que quiero mostrar
    $_SESSION['numeros'] = aleatorios();
    $_SESSION['aciertos'] = []; // numeros acertados
    $_SESSION['errores'] = 0; // numero de errores
    $_SESSION['intentos'] = 0; // numero de intentos
}



?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form method="post">
        <div>
            <label for="numero">Numero</label>
            <input type="number" name="numero" id="numero">
        </div>

        <div>
            <button type="submit" name="jugar">Jugar</button>
            <button type="submit" name="reiniciar">Reiniciar</button>
        </div>
    </form>
    <?php
    // mostrar los numeros acertados
    echo "<h2>Numero acertados</h2>";
    foreach ($_SESSION['aciertos'] as $numero) {
        echo $numero . "<br>";
    }
    // mostrar errores
    echo "<h2>Errores</h2>";
    echo $_SESSION['errores'];

    // mostrar intentos
    echo "<h2>Intentos</h2>";
    echo $_SESSION['intentos'];

    ?>
    <br>
    <?= $mensaje ?>

</body>

</html>