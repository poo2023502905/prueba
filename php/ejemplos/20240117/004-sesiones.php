<?php


/**
 * es una funcion para mostrar las variables en la vista
 * @param string $mensaje Titulo a mostrar
 * @param array $numeros Valores a mostrar
 */
function mostrar(string $mensaje = "---", ...$numeros): string
{
    $resultado = "<h2>{$mensaje}</h2>";
    $resultado .= "<table>";
    $resultado .= "<tr>";
    foreach ($numeros as $num) {
        $resultado .= ($num == "-") ? "<td>-</td>" : "<td class='acierto'>$num</td>";
        // $resultado .= "<td>$num</td>";
    }
    $resultado .= "</tr>";
    $resultado .= "</table>";
    return $resultado;
}

// inicializar la sesion
session_start();

// se trata de realizar un pequeño juego donde
// el usuario tiene que adivinar los 10 numeros del array
// creamos un formulario donde escribo un numero y cuando pulso
// enviar debe comprobar si el numero esta en el array
// si esta en el array me indica que he acertado y me lo muestra
// si fallo me cuenta un fallo y me debe mostrar el numero de fallos
// cuando adivine los 10 numeros la puntuacion sera el numero de intentos 
// para adivinar los 10 numeros

// una vez que adivina los 10 numeros debe borrar las variables de sesion
// colocar un boton para reiniciar el juego cuando quieras


// creo la variable numeros 
// leyendo los valores de la variable de sesion numeros
// la variable de sesion numeros tendra los numeros que me quedan por acertar
// si la variable de sesion numeros no existe introduzco
// en la variable numeros los 10 numeros
$numeros = $_SESSION["numeros"] ?? [
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10
];

// creo una variable por cada variable de sesion que voy a utilizar
// $_SESSION["aciertos"] donde almaceno cada numero que acierto en la misma posicion del array original
// la variable aciertos tiene o los numeros que llevo acertando o 10 guiones
$aciertos = $_SESSION["aciertos"] ?? array_fill(0, count($numeros), '-');

// $_SESSION["errores"] donde almaceno el numero de fallos
// si no existe la variable de session errores incializo los errores 0
$errores = $_SESSION["errores"] ?? 0;

// $_SESSION["intentos"] donde almaceno el numero de intentos
// si no existe la variable de session intentos incializo los intentos 0
$intentos = $_SESSION["intentos"] ?? 0;

// variable para controlar si seguimos jugando
// cuando final valga 1 necesito reiniciar el juego
// el juego se puede reiniciar cuando pulsa el boton reiniciar o cuando termino de adivinar todos
$final = 0;

// comprobar que he pulsado el boton de jugar
if (isset($_POST["jugar"])) {

    // control del minijuego
    // leo el numero si no esta vacio
    if (isset($_POST["numero"])) {
        // leo el numero del formulario 
        $numero = $_POST["numero"];

        // comprobar si el numero esta en el array  de numeros
        // que tiene solo los numeros que me quedan por acertar
        if (in_array($numero, $numeros)) { // comprueba si el numero esta en el array de numeros
            // si esta en el array me coloca el acierto en aciertos en la misma posicion 
            // que a encontrado
            // array_search devuelve la posicion del primer numero encontrado
            $aciertos[array_search($numero, $numeros)] = $numero;

            //elimino el numero acertado de numeros
            // la funcion unset es para eliminar variables
            // si lo utilizas en un array es capaz de eliminar un elemento del array
            // pero manteniendo los indices originales
            unset($numeros[array_search($numero, $numeros)]);
        } else {
            // añado un error
            $errores++;
        }
        // añado un intento
        $intentos++;

        // comprobar si se ha acabado el juego
        // si no hay mas numeros para adivinar
        if (count($numeros) == 0) {
            $final = 1; // coloco la variable a true para reiniciar el juego
        }
    }
    // vuelvo a colocar los valores en las variables de sesion
    $_SESSION["aciertos"] = $aciertos; // numeros que he acertado
    $_SESSION["errores"] = $errores; // numero de errores cometidos
    $_SESSION["intentos"] = $intentos; // numero de intentos
    $_SESSION["numeros"] = $numeros; // numeros que me quedan por acertar

} elseif (isset($_POST["reiniciar"])) { // si he pulsado el boton de reiniciar 
    $final = 1; // coloco la variable a true para reiniciar el juego
}


// compruebo si existe alguna condicion por la que se termina el juego
if ($final) {
    // borrar sesion completa (opcion 1)
    // session_unset();

    // borrar las variables de sesion (opcion 2)
    unset($_SESSION["numeros"]);
    unset($_SESSION["aciertos"]);
    unset($_SESSION["errores"]);
    unset($_SESSION["intentos"]);

    // inicializamos las variables
    $aciertos = array_fill(0, 10, '-');
    $errores = 0;
    $intentos = 0;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table {
            border: 1px solid black;
            border-collapse: collapse;
        }

        td {
            border: 1px solid black;
            width: 100px;
            text-align: center;
            vertical-align: middle;
            padding: 10px;
        }

        label {
            background-color: gray;
            color: white;
            padding: 10px;
        }

        form,
        .salida {
            margin: 20px;
        }

        input,
        button {
            padding: 10px;

        }

        h2 {
            text-transform: uppercase;
        }

        .acierto {
            background-color: gray;
            color: white;
        }
    </style>
</head>

<body>
    <form method="post">
        <div>
            <label for="numero">Numero</label>
            <input type="number" id="numero" name="numero">
        </div>
        <br>
        <div>
            <button type="submit" name="jugar">Enviar</button>
            <button type="submit" name="reiniciar">Reiniciar</button>
        </div>
    </form>
    <div class="salida">
        <?php
        // mostrar numeros acertados
        // var_dump($aciertos);
        echo mostrar("aciertos", ...$aciertos);
        // mostrar errores
        // var_dump($errores);
        echo mostrar("errores", $errores);
        // mostrar intentos
        // var_dump($intentos);
        echo mostrar("intentos", $intentos);
        ?>
    </div>
</body>

</html>