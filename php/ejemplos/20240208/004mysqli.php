<?php

// objetivo

// conectarme al servidor de base de datos mysql localhost
// seleccionar la base de datos personas
// realizar una consulta para mostrar la tabla Cliente
// utilizamos el metodo FETCH_ARRAY
// mostrar los registros de la tabla cliente
// cerrar conexión

// conexion

$conexion = new mysqli("localhost", "root", "", "personas");

// consulta
$resultados = $conexion->query("SELECT * FROM cliente");

// mostrar los resultados
// recorriendolos utilizando fetch_array
$contador = 0;
while ($registro = $resultados->fetch_array(MYSQLI_ASSOC)) {
    echo "<h2>Registro numero {$contador}</h2>";
    echo "<ul>";
    foreach ($registro as $nombreCampo => $valorCampo) {
        echo "<li>{$nombreCampo} : {$valorCampo}</li>";
    }
    echo "</ul>";
    $contador++;
}

// cerrar la conexion
$conexion->close();
