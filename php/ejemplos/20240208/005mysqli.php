<?php
//conectarse a la base de datos
$conexion = new mysqli('localhost', 'root', '', 'personas');
$conexion->set_charset("utf8");

$contador=0;
// realizo la consulta
$resultados=$conexion->query("SELECT p.idPersona, p.nombre, p.edad FROM persona p");

// quiero sacar solo un resultado con el primer registro
// con el while lee todos los registros y los pasa al array asociativo
// realizo el listado con un while 
// utilizando fech object 
// fech object te crea un objeto de la clase anonima
// esa clase es la que se denomina stdClass
// $registro=$resultados->fetch_object();

// var_dump($registro);

// la manera mas correcta es esta
$contador=0;
while ($registro=$resultados->fetch_object()) {
    echo "<h2>Registro $contador</h2>";
     echo "<ul>";
     echo " <li>idPersona: {$registro->idPersona}</li><br>";
     echo "<li>nombre: {$registro->nombre}</li> <br>";
     echo "<li>edad: {$registro->edad} </li><br>";
     echo "</ul>";
     $contador++;
     echo "<hr>";
}
// cierro la conexion
// apuntar a el primer registro o reiniciar el puntero
// de los resultados
$resultados->data_seek(0);
// ahora lo vamos hacer mediante un while y un foreach
$contador=0;
while($registro=$resultados->fetch_object()){
    echo "<h2>Registro {$contador}</h2>";
    echo "<ul>";
    foreach($registro as $clave => $valor){
        echo "<li>$clave: $valor</li>";
    }
    echo "</ul>";
    $contador++;
}
var_dump($registro);

$conexion->close();