<?php
// objetivo

// conectarme al servidor de base de datos mysql localhost
// seleccionar la base de datos personas
// realizar una consulta para mostrar la tabla persona
// utilizamos el metodo FETCH_ARRAY
// mostrar los registros de la tabla persona
// cerrar conexión

// conectar al servidor
$conexion = new mysqli('localhost', 'root', '', 'personas');

// realizo la consulta
$resultados = $conexion->query('SELECT * FROM persona');

/**
 * quiero para sacar los datos crear solamente un 
 * array unidimensional
 */

// realizo el listado con un while y el meotod fetch_array

// variable para mostrar el numero de registro
$contador = 0;
// crea un array asociativo con el primer registro
// para ello utilizo fetch_array
// con el while lee todos los registros y los pasa al array asociativo
while ($registro = $resultados->fetch_array(MYSQLI_ASSOC)) {
    echo "<h2>Registro {$contador}</h2>";
    // muestro el array asociativo
    echo "<ul>";
    echo "<li>idPersona: {$registro['idPersona']} </li>";
    echo "<li>nombre: {$registro['nombre']} </li>";
    echo "<li>edad: {$registro['edad']} </li>";
    echo "</ul>";
    $contador++;
}


// colocar el puntero de los resultados al primer registro de nuevo
// para ello utilizo el metodo data_seek
$resultados->data_seek(0);

// puedo realizar lo mismo con un while y un foreach
$contador = 0;
while ($registro = $resultados->fetch_array(MYSQLI_ASSOC)) {
    echo "<h2>Registro {$contador}</h2>";
    // muestro el array asociativo
    echo "<ul>";
    foreach ($registro as $nombreCampo => $valorCampo) {
        echo "<li>{$nombreCampo}: {$valorCampo}</li>";
    }
    echo "</ul>";
    $contador++;
}


// cerrar la conexion
$conexion->close();
