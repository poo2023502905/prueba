<?php

// objetivo

// conectarme al servidor de base de datos mysql localhost
// seleccionar la base de datos personas
// realizar una consulta para mostrar la tabla Cliente
// mostrar los registros de la tabla cliente
// cerrar conexión

// crear una conexion con el servidor utilizando la 
// clase mysqli
$conexion = new mysqli("localhost", "root", "", "personas");

// realizar una consulta
// utilizando el metodo query del objeto mysqli
$resultado = $conexion->query("select * from cliente"); // creo un objeto de tipo mysqli_result

// creo un array con los registros utilizando 
// el metodo fetch_all y la constante MYSQLI_ASSOC para obtener un array asociativo
$registros = $resultado->fetch_all(MYSQLI_ASSOC);

echo "<h1>Registros de la tabla cliente</h1>";
foreach ($registros as $indice => $registro) {
    echo "<h2>Registro {$indice}</h2>";
    echo "<ul>";
    foreach ($registro as $nombreCampo => $valorCampo) {
        echo "<li>{$nombreCampo} : {$valorCampo}</li>";
    }
    echo "</ul>";
}

// cerrar la conexion
// utilizando el metodo close
$conexion->close();
