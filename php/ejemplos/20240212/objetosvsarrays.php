<?php


class Persona
{
    public $nombre;
    public $apellido;
    public $edad;

    public function saludar()
    {
        return "hola soy " . $this->nombre . " y tengo " . $this->edad . " años";
    }
}


$persona1 = new Persona();
$persona1->nombre = "Jorge";
$persona1->apellido = "Lopez";
$persona1->edad = 32;

echo $persona1->nombre;


$vector = get_object_vars($persona1);

var_dump($vector);
var_dump($persona1);


echo $persona1->saludar();
