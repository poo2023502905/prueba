<?php
// creeis un objeto de clase Texto de ramon y otro de profesor
// llamar al metodo de mostrar de cada objeto creado
spl_autoload_register(function ($clase) {
    include $clase . '.php';
});

$objeto1 = new \clases\ramon\Texto();
$objeto2 = new \clases\profesor\Texto();

echo $objeto1->mostrar();
echo $objeto2->mostrar();
