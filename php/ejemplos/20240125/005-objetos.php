<?php
// importo la clase al espacio de nombres actual

use clases\profesor\Texto;
use clases\ramon\Texto as RamonTexto;

spl_autoload_register(function ($clase) {
    include $clase . '.php';
});


// crear objeto de tipo texto

$objeto = new Texto();
$objeto1 = new RamonTexto();

echo $objeto->mostrar();
echo $objeto1->mostrar();
