<?php

namespace clases\profesor;

class Numeros
{
    public array $numeros;
    public function __construct(array $numeros = [])
    {
        $this->numeros = $numeros;
    }

    public function calcularModa(): float
    {
        // tenga en cuenta que puede haber mas de 1 numero con el
        // mismo numero de repeticiones
        $frecuenciaMaxima = 0;
        $valoresMaximasRepeticiones = [];
        $moda = 0;

        // opcion 1
        // con array_count_values y foreach
        $frecuencias = array_count_values($this->numeros);

        // calculo la frecuencia maxima
        foreach ($frecuencias as $indice => $frecuencia) {
            if ($frecuencia > $frecuenciaMaxima) {
                $frecuenciaMaxima = $frecuencia;
            }
        }

        // realizo lo mismo que el foreach anterior
        $frecuenciaMaxima = max($frecuencias);

        // coloco en el array de valores maximos repeticiones
        // los valores de la frecuencia maxima
        // estos valores son los indices del array frecuencias
        foreach ($frecuencias as $indice => $frecuencia) {
            if ($frecuencia == $frecuenciaMaxima) {
                $valoresMaximasRepeticiones[] = $indice;
            }
        }

        // para calcular la moda realizo la media del
        // array de valores maximos repeticiones
        $moda = array_sum($valoresMaximasRepeticiones) / count($valoresMaximasRepeticiones);

        return $moda;
    }
}
