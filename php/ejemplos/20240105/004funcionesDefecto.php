<?php
/**
* Crea un elemento HTML con el contenido y la etiqueta proporcionados.
* * @param string $contenido El contenido del elemento HTML.
* @param string $etiqueta El nombre de la etiqueta  Por defecto es "div".
* @return string El elemento HTML como una cadena de texto.
*/

function crearEtiqueta($contenido, $etiqueta="div"){
    return "<{$etiqueta}>{$contenido}</{$etiqueta}>";
}

echo crearEtiqueta("hola mundo");
echo crearEtiqueta("hola mundo", "p");
?>