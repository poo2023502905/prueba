<?php
$resultado = "";
// si he pulsado el boton de enviar
if ($_POST) {
    // mostrar los datos enviados
    var_dump($_POST);
    // creo un string con los colores seleccionados
    $resultado = implode(",", $_POST);
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form method="post">
        <input type="checkbox" name="rojo" value="rojo" id="rojo">
        <label for="rojo">Rojo</label>
        <input type="checkbox" name="azul" value="azul" id="azul">
        <label for="azul">Azul</label>
        <input type="checkbox" name="verde" value="verde" id="verde">
        <label for="verde">Verde</label>
        <button type="submit">Enviar</button>
    </form>
    <?= $resultado ?>
</body>

</html>