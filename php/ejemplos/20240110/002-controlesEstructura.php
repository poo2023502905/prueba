<?php
$resultado = "";
if ($_POST) {
    $resultado = isset($_POST["frutas"]) ? implode(",", $_POST["frutas"]) : "";
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Usar cuadros de lista</h1>
    <form method="post">
        <div>
            <label for="frutas">Elija sus frutas preferidas</label>
            <br>
            <select name="frutas[]" id="frutas" multiple>
                <option>Manzana</option>
                <option>Naranja</option>
                <option>Pera</option>
                <option>Pomelo</option>
            </select>
        </div>
        <br>
        <div>
            <button>Enviar</button>
        </div>
    </form>

    <?= $resultado ?>
</body>

</html>