<?php

use clases\Planeta;

spl_autoload_register(function ($clase) {
    require $clase . ".php";
});

$planeta1 = new Planeta();
$planeta2 = new Planeta("mercurio");
$planeta3 = new Planeta("venus", 3, 234, 23, 123, 23, "tipo1", true);
$planeta4 = new Planeta("tierra", 3, 234, 23, 123, 23000145785, "tipo1", false);

echo $planeta1->imprimir();
echo $planeta2->imprimir();
echo $planeta3->imprimir();
echo $planeta4->imprimir();

echo $planeta4->densidad();
echo "<br> El planeta es " . ($planeta4->esExterior() ? "exterior" : "interior");
