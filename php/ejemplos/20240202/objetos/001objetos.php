<?php
class Padre
{
    public string $nombre;
    public string $apellidos;
    private string $tipo;
    public int $edad;
    protected int $altura;

    public int $nivel;


    public function __construct(string $nombre, string $apellidos, int $edad, int $altura)
    {
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->tipo = "padre";
        $this->edad = $edad;
        $this->altura = $altura;
        $this->nivel = 1;
    }

    public function presentarme()
    {
        return "Soy el padre";
    }
    public function asignar(string $nombre, string $apellidos)
    {
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
    }

    public function getEdad()
    {
        return $this->edad;
    }

    public function getTipo()
    {
        return $this->tipo;;
    }

    public function setTipo(string $tipo): self
    {
        $this->tipo = $tipo;
        return $this;
    }
}

class Hijo extends Padre
{
    public string $tipo;
    public function __construct(string $nombre, string $apellidos, int $edad, int $altura)
    {
        $this->tipo = "hijo";
        $this->altura = 0; // al ser la altura protected, puedo acceder pero es la misma que la de la superclase
        parent::__construct($nombre, $apellidos, $edad, $altura);
    }
    public function presentarme(): string
    {
        return "Soy el hijo";
    }

    public function getTipo(): string
    {
        return $this->tipo;
    }

    public function getTipoPadre(): string
    {
        return parent::getTipo();
    }

    public function asignar(string $nombre, string $apellidos)
    {
        parent::asignar($nombre, $apellidos);
        $this->tipo = "hijo con datos";
        $this->setTipo('padre con hijo con datos');
    }
}

$hijo = new Hijo("Pepe", "Perez", 20, 180);

var_dump($hijo);

echo $hijo->presentarme();

echo $hijo->getTipo();
echo $hijo->tipo;

$padre = new Padre("Juan", "Perez", 60, 180);
var_dump($padre);

echo $padre->presentarme();

echo $padre->getTipo();

// echo $padre->altura; //error
// echo $padre->tipo; // error



