<?php
spl_autoload_register(function ($clase) {
    require 'clases/' . $clase . ".php";
});

// quiero crear un gato
$gato1 = new Gato();
// le cambio el peso que tenia por defecto
$gato1->peso = 6;

// creo otro gato
// con nombre, color y sin pelo
// como no le paso el peso me coge 5
$gato2 = new Gato('jorge', 'negro', 'no');

var_dump($gato1);
var_dump($gato2);
