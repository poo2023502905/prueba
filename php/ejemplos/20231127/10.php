<?php
// array bidimensional
$datos = [
    [
        "id" => 1,          // $datos[0]["id"]
        "nombre" => "Eva", // $datos[0]["nombre"]
    ],
    [
        "id" => 2, // $datos[1]["id"]
        "nombre" => "Adan", // $datos[1]["nombre"]
    ],
];

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <table border="1">
        <tr>
            <td>Id</td>
            <td>Nombre</td>
        </tr>
        <tr>
            <td><?= $datos[0]["id"] ?></td>
            <td><?= $datos[0]["nombre"] ?></td>
        </tr>
        <tr>
            <td><?= $datos[1]["id"] ?></td>
            <td><?= $datos[1]["nombre"] ?></td>
        </tr>
    </table>

</body>

</html>