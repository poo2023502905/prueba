<?php

// opcion 1
// use clases\herencia\Animal;
// use clases\herencia\Perro;
// use clases\herencia\Vaca;
// use clases\herencia\Persona;

// opcion 2
use clases\herencia\{Animal, Perro, Vaca, Persona};

require_once 'autoload.php';

$vaca1 = new Vaca("ernesta");
var_dump($vaca1);

$perro1 = new Perro();

$perro2 = new Perro("tito");

$animal = new Animal();

$persona1 = new Persona();

var_dump($perro1);
var_dump($perro2);
var_dump($animal);
var_dump($persona1);

// llama al metodo descripcion de la clase Perro, 
// como la clase perro no tiene ese metodo lo busca en el padre (Animal)
echo $perro2->descripcion();

// llama al metodo descripcion de la clase Vaca
// como la clase vaca tiene ese metodo lo utiliza
// la clase Animal tambien tiene ese metodo pero la clase Vaca lo ha sobreescrito
echo $vaca1->descripcion();
