<?php

namespace clases\herencia;

class Perro extends Animal // hereda las propiedades de Animal
{
    public bool $vacunado;
    public ?Persona $persona;

    public function __construct(string $nombre = "", float $peso = 0, string $color = "", bool $vacunado = false, ?Persona $persona = null)
    {
        $this->nombre = $nombre;
        $this->peso = $peso;
        $this->color = $color;
        $this->vacunado = $vacunado;
        $this->persona = $persona;
    }
}
