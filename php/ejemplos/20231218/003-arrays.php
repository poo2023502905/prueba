<?php

// funcion para mostrar los 
// datos de un array bidimensional
// donde en la primera dimension tengo los 
// registros y en la segunda dimension los campos
// mostramos los resultados en una tabla
// la funcion la llamamos mostrarDatos
// y recibe como argumento el array bidimensional
// retorna la tabla a mostrar

// <table>
//     <tr>
//         <td>1</td>
//         <td>Pepe</td>
//         <td>Perez</td>
//         <td>20</td>
//     </tr>
//     <tr>
//         <td>2</td>
//         <td>Juan</td>
//         <td>Lopez</td>
//         <td>21</td>
//     </tr>
// </table>

function mostrarDatos(array $datos): string
{
    $html = "<table>";
    // registros
    foreach ($datos as $registro) {
        $html .= "<tr>";

        // principio mostrar campos
        foreach ($registro as $dato) {
            $html .= "<td>{$dato}</td>";
        }
        // fin mostrar campos

        $html .= "</tr>";
    }

    $html .= "</table>";
    return $html;
}

function mostrarDatosV1(array $datos): string
{
    $html = "<table>";
    foreach ($datos as $registro) {
        $html .= "<tr>";
        array_walk($registro, function ($valor) use (&$html) {
            $html .= "<td>{$valor}</td>";
        });
        $html .= "</tr>";
    }

    $html .= "</table>";
    return $html;
}

function mostrarDatosV2(array $datos): string
{
    $html = "<table>";
    array_walk($datos, function ($registro) use (&$html) {
        $html .= "<tr>";
        array_walk($registro, function ($valor) use (&$html) {
            $html .= "<td>{$valor}</td>";
        });
        $html .= "</tr>";
    });

    $html .= "</table>";
    return $html;
}



// funcion para mostrar los 
// datos de un array bidimensional
// donde en la primera dimension tengo los 
// registros y en la segunda dimension los campos
// mostramos los resultados en una tabla
// la funcion la llamamos mostrarDatosCabecera
// y recibe como argumento el array bidimensional
// retorna la tabla a mostrar con cabeceras
// <table>
//     <thead>
//         <tr>
//             <th>Id</th>
//             <th>Nombre</th>
//             <th>Apellidos</th>
//             <th>Edad</th>
//         </tr>
//     </thead>
//     <tbody>
//         <tr>
//             <td>1</td>
//             <td>Pepe</td>
//             <td>Perez</td>
//             <td>20</td>
//         </tr>
//         <tr>
//             <td>2</td>
//             <td>Juan</td>
//             <td>Lopez</td>
//             <td>21</td>
//         </tr>
//     </tbody>
// </table>

function mostrarDatosCabecera(array $datos): string
{
    $html = "<table>";
    // voy a realizar la cabecera de la tabla
    $cabecera = "<thead><tr>";

    $indices = array_keys($datos[0]);

    // opcion 1 para juntar los indices y crear un
    // string con la cabecera
    // foreach ($indices as $valor) {
    //     $cabecera .= "<th>{$valor}</th>";
    // }

    // opcion 2 para juntar los indices y crear un
    // string con la cabecera
    array_walk($indices, function ($valores) use (&$cabecera) {
        $cabecera .= "<th>{$valores}</th>";
    });

    $cabecera .= "</tr></thead>";

    // añado la cabecera a la tabla
    $html .= $cabecera;

    // para añadir los registros a la tabla copio
    // lo que tengo en las funciones anteriores
    foreach ($datos as $registro) {
        $html .= "<tr>";
        array_walk($registro, function ($valor) use (&$html) {
            $html .= "<td>{$valor}</td>";
        });
        $html .= "</tr>";
    }

    $html .= "</table>";
    return $html;
}
