<?php

//  funcion llamada mostrarArray que recorre un array pasado como 
// primer parametro y retorna una tabla HTML
// donde en la primera columna esta el indice y en la segunda el valor
// el indice debe estar como th y el valor como td
// utilizar para recorrer el array for
// solo admite arrays unidimensionales de tipo enumerado y secuenciales

function mostrarArray(array $datos): string
{
    $html = "<table>";
    for ($c = 0; $c < count($datos); $c++) {
        $html .= "<tr><th>$c</th><td>$datos[$c]</td></tr>";
    }
    $html .= "</table>";

    return $html;
}


//  funcion llamada mostrarArrayV1 que recorre un array pasado como 
// primer parametro y retorna una tabla HTML
// donde en la primera columna esta el indice y en la segunda el valor
// el indice debe estar como th y el valor como td
// utilizar para recorrer el array for con la funcion array_values y array_keys
// para que admita cualquier tipo de array (asociativo o enumerado)

function mostrarArrayV1(array $datos): string
{
    $html = "<table>";
    // en valores meto los valores del array pasado como argumento
    $valores = array_values($datos);
    // en indices meto los indices del array pasado como argumento
    $indices = array_keys($datos);

    // $colores = [
    //     'red' => 'rojo',
    //     'blue' => 'azul'
    // ];
    // $valores = ['rojo', 'azul'];
    // $indices = ['red','blue'];
    for ($c = 0; $c < count($valores); $c++) {
        $html .= "<tr><th>$indices[$c]</th><td>$valores[$c]</td></tr>";
    }

    $html .= "</table>";

    return $html;
}

//  funcion llamada mostrarArray2 que recorre un array pasado como 
// primer parametro y retorna una tabla HTML
// donde en la primera columna esta el indice y en la segunda el valor
// el indice debe estar como th y el valor como td
// utilizar para recorrer el array foreach
// solo admite arrays unidimensionales 

function mostrarArray2(array $datos): string
{
    $html = "<table>";
    foreach ($datos as $indice => $valor) {
        $html .= "<tr><th>$indice</th><td>$valor</td></tr>";
    }
    $html .= "</table>";
    return $html;
}


//  funcion llamada mostrarArray3 que recorre un array pasado como 
// primer parametro y retorna una tabla HTML
// donde en la primera columna esta el indice y en la segunda el valor
// el indice debe estar como th y el valor como td
// utilizar para recorrer el array array_walk

function mostrarArray3(array $datos): string
{
    $html = "<table>";
    array_walk($datos, function ($valor, $indice) use (&$html) {
        //global $html; // esto es otra opcion en vez de use
        $html .= "<tr><th>{$indice}</th><td>{$valor}</td></tr>";
    });
    $html .= "</table>";
    return $html;
}



//  funcion llamada mostrarArray3V1 que recorre un array pasado como 
// primer parametro y retorna una tabla HTML
// donde en la primera columna esta el indice y en la segunda el valor
// el indice debe estar como th y el valor como td
// utilizar para recorrer el array array_walk junto con implode
function mostrarArray3V1(array $datos): string
{
    $html = "<table>";
    array_walk($datos, function (&$valor, $indice) {
        $valor = "<tr><th>{$indice}</th><td>{$valor}</td></tr>";
    });
    $html .= implode("", $datos);
    $html .= "</table>";
    return $html;
}

//  funcion llamada mostrarArray4 que recorre un array pasado como 
// primer parametro y retorna una tabla HTML
// donde en la primera columna esta el indice y en la segunda el valor
// el indice debe estar como th y el valor como td
// utilizar para recorrer el array array_map e implode

function mostrarArray4(array $datos): string
{
    $html = "<table>";
    // creando un array con los valores
    $valores = array_values($datos);

    // creo otro array con los indices
    $indices = array_keys($datos);

    // creo otro array donde cada elemento
    // es una fila de la tabla con el indice y el valor
    $datos1 = array_map(function ($valor, $indice) {
        return "<tr><th>{$indice}</th><td>{$valor}</td></tr>";
    }, $valores, $indices);

    $html .= implode("", $datos1);
    $html .= "</table>";
    return $html;
}
