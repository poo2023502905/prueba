<?php
// rojo ==> r
// amarillo ==> a
// verde ==> v

$color = "a";
//$color = $_GET["color"];
$salida = "";

// mediante if comprobar a que color corresponde la inicial
if ($color == 'r') {
    $salida = "Rojo";
} elseif ($color == 'a') {
    $salida = "Amarillo";
} elseif ($color == 'v') {
    $salida = "Verde";
}

echo $salida;
