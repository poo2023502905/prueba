<?php
/**
 * 
 * Calcular el descuento de un numero de unidades pedido
 * 1-10 : descuento 0
* 11-20 : descuento 10
*  21-30 : descuento 20
* mayor o igual que 31 : descuento 30
 * @param int $numero
 * @return int descuento calculado
 *
 */


funcion calcularDescuento(int $numero): int

{
    $descuento=0;
    if ( $numero<=10)
{
    $descuento=0;
}
else if ($numero<=20)
{
    $descuento=10;
}
else if ($numero<=30)
{
    $descuento=20;
}
else if ($numero>=31)
{
    $descuento=30;
}

return $descuento;
}

$solucion=calcularDescuento(20);
echo 
?>