<?php
$a = 10;
$b = 23.4;
$c = true;
$d = [1, 2, 4];

$tipo = gettype($a);

echo "{$tipo}<br>";

$tipo = gettype($b);

echo "{$tipo}<br>";

$tipo = gettype($c);

echo "{$tipo}<br>";

// imprimo el tipo sin utilizar la variable
echo gettype($d) . "<br>";
