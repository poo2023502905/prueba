﻿DROP DATABASE IF EXISTS aplicacionFavoritos;

CREATE DATABASE aplicacionfavoritos;

USE aplicacionfavoritos;

CREATE TABLE favoritos (
    id int AUTO_INCREMENT, url varchar(300), titulo varchar(200), descripcion text, categorias varchar(200), imagen varchar(200), PRIMARY KEY (id)
);

INSERT INTO
    favoritos (
        url, titulo, descripcion, categorias
    )
VALUES (
        'alpeformacion.es', 'Pagina Alpe', 'Pagina de la empresa', 'formacion'
    ),
    (
        'formacion.es', 'Pagina de formacion', 'Pagina de la empresa', 'formacion'
    ),
    (
        'google.es', 'Pagina del buscador', 'Pagina de la empresa', 'buscador'
    ),
    (
        'youtube.es', 'Pagina del video', 'Pagina de la empresa', 'video'
    );

SELECT * FROM favoritos;