    <div class="py-5 bg-dark text-white text-center">
        <i class="bi bi-person-circle" style="font-size: 4rem;"></i>
        <h1 class="display-4"><?= $titulo ?></h1>
        <p class="lead"><?= $subtitulo ?></p>
    </div>
    <div class="p-5 bg-dark text-white">
        <div class="lead"><?= $salida ?></div>
    </div>