<?php
session_start();

// Asignar algunas variables de sesión
$_SESSION['usuario'] = 'Ramon';

// Imprimir variables antes de unset
echo 'Usuario: ' . $_SESSION['usuario'] . '<br>';

// Unset (eliminar) todas las variables de sesión
session_unset();


$_SESSION['rol'] = 'admin';

// Imprimir variables después de unset
echo 'Usuario después de unset: ' . $_SESSION['usuario'] . '<br>'; // produce warning
echo 'Rol después de unset: ' . $_SESSION['rol'] . '<br>'; // muestra 'admin'
