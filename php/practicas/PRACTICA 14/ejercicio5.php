<?php

// crear una función que le pasas tres numeros y te devuelva el producto

function multiplicar_tres_numeros($num1, $num2, $num3) {
    return $num1 * $num2 * $num3;
}

// Llamamos a la función para sumar los numeros
echo multiplicar_tres_numeros(10, 20, 30);