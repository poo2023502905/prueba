<?php

// crear una función que le pasas un array y te lo imprime al revés

function seImprimeAlReves($array) {
    for ($i = count($array) - 1; $i >= 0; $i--) {
        echo $array[$i] . " ";
    }
}   

// Llamamos a la función para imprimir los números
seImprimeAlReves([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
    
