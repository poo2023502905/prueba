<?php
// crear una función que le pasas un array y te lo devuelve ordenado

function ordenarArray($array) {
    sort($array);
    return $array;
}

// Llamamos a la función para ordenar el array
$array_ordenado = ordenarArray($array);

// Imprimimos el array ordenado
echo implode(", ", $array_ordenado);


