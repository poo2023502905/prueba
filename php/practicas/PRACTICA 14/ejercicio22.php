<?php
// crear una función que le pasas dos arrays del mismo tamaño
// y me devolverá el número de elementos que coinciden


function contar_coincidencias($array1, $array2) {
    $cont = 0;
    for ($i = 0; $i < count($array1); $i++) {
        if (in_array($array1[$i], $array2)) {
            $cont++;
        }
    }
    return $cont;
}
// llamamos a la función

echo contar_coincidencias([1, 2, 3, 4, 5], [3, 4, 5, 6, 7]);

