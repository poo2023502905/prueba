<?php
function calcular($num1, $num2, $operacion) {
    switch ($operacion) {
        case 'suma':
            return $num1 + $num2;
        case 'producto':
            return $num1 * $num2;
        case 'resta':
            return $num1 - $num2;
        default:
            return "Operación no válida. Debe ser 'suma', 'producto' o 'resta'.";
    }
}

// Ejemplo de uso
$numero1 = 10;
$numero2 = 5;
$operacion_suma = 'suma';
$operacion_producto = 'producto';
$operacion_resta = 'resta';

$resultado_suma = calcular($numero1, $numero2, $operacion_suma);
$resultado_producto = calcular($numero1, $numero2, $operacion_producto);
$resultado_resta = calcular($numero1, $numero2, $operacion_resta);

echo "Resultado de la suma: " . $resultado_suma . "\n";
echo "Resultado del producto: " . $resultado_producto . "\n";
echo "Resultado de la resta: " . $resultado_resta . "\n";



