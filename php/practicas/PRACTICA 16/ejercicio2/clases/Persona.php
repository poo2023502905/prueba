<?php

// espacio de nombres 
namespace clases;

class Persona
{
    public string $nombre;
    public string $apellidos;
    public string $numeroDocumentoIdentidad;
    public int $añoNacimiento;
    public string $paisNacimiento;

    public string $genero;

    // constructor
    public function __construct(string $nombre, string $apellidos, string $numeroDocumentoIdentidad, int $añoNacimiento, string $paisNacimiento, string $genero)
    {
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->numeroDocumentoIdentidad = $numeroDocumentoIdentidad;
        $this->añoNacimiento = $añoNacimiento;
        $this->paisNacimiento = $paisNacimiento;
        $this->genero = $genero;
    }

    public function imprimir(): string
    {
        $salida = "<h2>Datos</h2>";
        $salida .= "<p>Nombre: " . $this->nombre . "</p>";
        $salida .= "<p>Apellidos: " . $this->apellidos . "</p>";
        $salida .= "<p>Numero de documento de identidad: " . $this->numeroDocumentoIdentidad . "</p>";
        $salida .= "<p>Año de nacimiento: " . $this->añoNacimiento . "</p>";
        $salida .= "<p>Pais de nacimiento: " . $this->paisNacimiento . "</p>";
        $salida .= "<p>Genero: " . $this->genero . "</p>";
        return $salida;
    }
}
