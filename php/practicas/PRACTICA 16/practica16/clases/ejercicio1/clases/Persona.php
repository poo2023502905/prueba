<?php
namespace clases;
class Persona{
    public string $nombre;
    public string $apellidos;
    public string $dni;

    public int $anyoNacimiento;

    public function __construct(string $nombre, string $apellidos,
     string $dni, int $anyoNacimiento){
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->dni = $dni;
        $this->anyoNacimiento = $anyoNacimiento;
        
    }
    public function imprimir(): string{
        $salida="<h2>Datos</h2>";
        $salida.="<p>Nombre: ".$this->nombre."</p>";
        $salida.="<p>Apellidos: ".$this->apellidos."</p>";
        $salida.="<p>DNI: ".$this->dni."</p>";
        $salida.="<p>Año de nacimiento: ".$this->anyoNacimiento."</p>";
        return $salida;
    }
}
