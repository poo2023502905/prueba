<?php
$datos=[
    [
        "nombre"=>"Eva",
        "edad"=>50
    ],
    [
        "nombre"=>"Jose",
        "edad"=>40,
        "peso"=>80,
    ],
];
// Introducir a Lorena de 80 años y con una altura de 175, realizarlo directamente
$datos[
"nombre"=>"Lorena",
"altura"=>175,
];
// Introducir a Luis de 20 años y con un peso de 90 y a Oscar de 23 años
// realizarlo mediante la función push
array_push($datos
[
    "nombre"=>"Luis",
    "edad"=>20,
],
[
    "nombre"=>"Oscar"
    "edad"=>20,
]
);