<?php
// desarrollar una función que nos permita crear un array
// creando color aletario en RGB
// a la función le debemos pasar el numero de colores a generar
// nos devolvera un array con estos colores
// debe aceptar un seundo parámetro opcional
// que si le pasamos el valor true con la # al principio del color
// 

/**
 * Genera un array de colores aleatorios.
 *
 * @param int $numero El número de colores a generar.
 * @param bool $almohadilla Opcional. Indica si se debe incluir un '#' en el código hexadecimal del color. El valor por defecto es true.
 * @return array Un array de colores generados.
 * si la colocamos false no colocara la #
 **/



function generaColores ($numero, $almohadilla=true){
    $colores=array();
    for ($n=0; $n<$numero; $n++){
        $r=mt_rand(0,255);
        $g=mt_rand(0,255);
        $b=mt_rand(0,255);
        if ($almohadilla==true){
            $colores[]="#".dechex($r).dechex($g).dechex($b);
        }else{
            $colores[]="rgb($r,$g,$b)";
        }

            
    }
    return $colores;   
}



?>