<?php

/**
 * Obtiene una lista de archivos en el directorio especificado,
 * y los ordena alfabéticamente.
 *
 * @param string $directorio El directorio del que se obtienen los archivos. Por defecto, el directorio actual.
 * @return array Un array con los nombres de los archivos, ordenados alfabéticamente.
 */

function ficherosOrdenadosAlfabeticamente($directorio = '.') {

    $ficheros = scandir($directorio);
    sort($ficheros);
    return $ficheros;   
}
var_dump(ficherosOrdenadosAlfabeticamente('.'));
?>