<?php
namespace clases\ejercicio2;
// creamos un usuario con 
class Usuario{
    // propiedades
    public int $idUsuario;
    public string $nombre;
    public string $email;
    public int $edad;
    public bool $activo;

    // constructor
    public function __construct(int $idUsuario, string $nombre, string $email, int $edad, bool $activo){
        $this->idUsuario = $idUsuario;
        $this->nombre = $nombre;
        $this->email = $email;
        $this->edad = $edad;
        $this->activo = $activo;
    }
    // creamos los metodo mostrar informacion
    public function mostrarInformacion() : string
    {
        $salida= "<h2>Datos</h2><br>";
         $salida.="Nombre: " . $this->nombre . "<br>";
         $salida.="Edad: " . $this->edad . "<br>";
         $salida.="Activo: " . $this->activo . "<br>";
         return $salida;
    }
    // creamos el metodo activar usuario
    public function activarUsuario(): void
    {
        $this->activo = true;
    }
    // creamos el metodo cambiar edad
    public function cambiarEdad( int $nuevaEdad): self
    {
        $this->edad = $nuevaEdad;
        return $this;
    }


}
